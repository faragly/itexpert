window.$ = window.jQuery = require('jquery');
require('bootstrap');
require('storageapi'); // для работы с localStorage
global.JBox = require('jbox'); // для нотификаций
global.App = require('./view');

(function($){
  'use strict';

  jQuery(document).ready(function($) {
    var app = new App({delay: 1500}),
      // опции jbox для всплывающих нотификаторов
      noticeOptions = {
        attributes: {
          x: 'right',
          y: 'bottom'
        },
        theme: 'NoticeBorder',
        color: 'green',
        animation: {
          open: 'slide:bottom',
          close: 'slide:right'
        }
      },
      // кэшируем табы в переменную
      tabs = $('#wrapper > ul.nav-tabs'),
      activeTab = $('> li.active > a', tabs).attr('href').replace('#', ''),
      // заголовки модальных окон при создании и редактировании
      modalText = {
        persons: {create: 'Create person', edit: 'Edit person'},
        users: {create: 'Create user', edit: 'Edit user'},
        positions: {create: 'Create position', edit: 'Edit position'},
        departments: {create: 'Create department', edit: 'Edit department'},
        companies: {create: 'Create company', edit: 'Edit company'},
        button: {create: 'Create', edit: 'Save'}
      };

    app.read();

    // действие создать или сохранить в модальном окне
    $(document).on('click', '.modal[role=dialog] button[role=button]', function(e) {
      var id = $('#create'+app.capitalize(activeTab)+'Modal').data('edit-id'),
        createPromise = $.type(id) !== 'undefined' ? app.create(e, id): app.create(e);

      $.when(createPromise).done(function(data){
        new JBox('Notice', $.extend(noticeOptions, {title: data.title, content: data.content}));
      });
    });

    // редактирование элемента
    $(document).on('click', 'button[name=edit]', function(e) {
      var modal = $('#create'+app.capitalize(activeTab)+'Modal').data('edit', true),
        tableRow = $(e.target).parents('tr');
      modal.data('edit-id', $('td:first', tableRow).text()).modal('show');
      app.setFormData($('form', modal), tableRow);
    });

    // удаление элемента с нотификацией
    $(document).on('click', 'button[name=remove]', function(e){
      if(confirm('Are you sure?')) {
        $(e.target).prop("disabled", true).prev().prop("disabled", true);
        $.when(app.remove(e)).done(function(data){
          new JBox('Notice', $.extend(noticeOptions, {title: data.title, content: data.content}));
        });
      }
    });

    // срабатывает при переключении вкладки, устанавливает activeTab, делает запрос чтения
    tabs.on('show.bs.tab', function(e) {
      activeTab = $(e.target).attr('href').replace('#', '');
      app.read(activeTab);
    });

    // модальное окно, создание и редактирование
    $('.modal[role=dialog]').on('show.bs.modal', function (e) {
      // если создаем элемент, то очищаем текстовые поля
      var edit = $.type($(this).data('edit')) === 'boolean' ? $(this).data('edit') : false;
      if (!edit) {
        $('form :input', this).val('');
      }
      // устанавливаем заголовок модального окна и button действия в зависимости от режима (создание/редактирование)
      $('h4.modal-title', this).text(edit ? modalText[activeTab].edit : modalText[activeTab].create);
      $('button[role=button]', this).text(edit ? modalText.button.edit : modalText.button.create);
    }).on('shown.bs.modal', function (e) {
      // устанавливаем фокус на первый
      $(':input:first', this).focus();
    }).on('hidden.bs.modal', function (e) {
      // при закрытии устанавливаем edit = false
      $(this).data('edit', false);
      // и удаляем data-edit-id при закрытии
      $.removeData(this, 'edit-id');
    });

  });
})(jQuery);